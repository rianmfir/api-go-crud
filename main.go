package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Customer struct {
	Id      string `json:"Id"`
	Nama    string `json:"nama"`
	Alamat  string `json:"alamat"`
	Telepon string `json:"telepon"`
}

var Customers []Customer

func returnAllCustomers(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: returnAllCustomer")
	json.NewEncoder(w).Encode(Customers)
}

func returnSingleCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]

	for _, customer := range Customers {
		if customer.Id == key {
			json.NewEncoder(w).Encode(customer)
		}
	}
	fmt.Println("Endpoint Hit: returnSingleCustomer")
}

func createNewCustomer(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	var customer Customer
	json.Unmarshal(reqBody, &customer)
	Customers = append(Customers, customer)
	json.NewEncoder(w).Encode(customer)

	fmt.Println("Endpoint Hit: createNewCustomer")
}

func updateCustomer(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: updateCustomer")
	w.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	key := vars["id"]

	for index, customer := range Customers {
		if customer.Id == key {
			Customers = append(Customers[:index], Customers[index+1:]...)

			var cust Customer

			_ = json.NewDecoder(r.Body).Decode(&cust)
			cust.Id = key
			Customers = append(Customers, cust)
			json.NewEncoder(w).Encode(&cust)

			return
		}
	}
	json.NewEncoder(w).Encode(Customers)

}

func deleteCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]

	for index, customer := range Customers {
		if customer.Id == key {
			Customers = append(Customers[:index], Customers[index+1:]...)
		}
	}
	fmt.Println("Endpoint Hit: deleteCustomer")
}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/customers", returnAllCustomers)
	myRouter.HandleFunc("/customer", createNewCustomer).Methods("POST")
	myRouter.HandleFunc("/customers/{id}", updateCustomer).Methods("PUT")
	myRouter.HandleFunc("/customers/{id}", deleteCustomer).Methods("DELETE")
	myRouter.HandleFunc("/customers/{id}", returnSingleCustomer)

	log.Fatal(http.ListenAndServe(":10000", myRouter))
}

func main() {
	Customers = []Customer{
		Customer{
			Id:      "1",
			Nama:    "Rian",
			Alamat:  "Bandung",
			Telepon: "089938474",
		},
		Customer{
			Id:      "2",
			Nama:    "Firdaus",
			Alamat:  "Depok",
			Telepon: "088812288474",
		},
	}
	handleRequests()
}
